import pandas as pd
import sys
import argparse

default_input_mirrors_filename      = 'list_of_freedesktop-sdk_mirrors.csv'
default_input_urls_filename         = 'list_of_freedesktop-sdk_source_urls.csv'
default_output_manifest_filename    = 'mirrors-manifest-freedesktop-sdk.csv'
default_problem_sources_filename    = 'mirrors-unmirrored-sources.csv'
default_problem_mirrors_filename    = 'mirrors-unused-mirrors.csv'
default_allow_list_filename         = 'mirrors-allow-list.csv'

red_ = u'\u001b[31m'
unred_ = u'\u001b[0m'

DESCRIBE_TEXT = '''
Takes two cvs files with the list of source URLs, and the list of mirrors, for freedesktop-sdk.
Combines the data to match sources with mirrors.
Outputs 3 files (by default)
    The main list   - listing all the matched URLs with the appropriate mirrors.
    Problem_sources - listing any source URLs that don't seem to be mirrored correctly.
    Problem_mirrors - listing any mirrors that don't seem to be used.
'''
parser = argparse.ArgumentParser(
        formatter_class = argparse.ArgumentDefaultsHelpFormatter,
        description = DESCRIBE_TEXT
        )
parser.add_argument('-v', '--verbose', action = 'store_true', 
        help = 'prints out the first 5 identified un-mirrored sources, and the first 5 identified unused mirrors')
parser.add_argument('-l', '--skip_main_manifest', action = 'store_true', 
        help = 'don\'t produce the main mainifest/list output')
parser.add_argument('-s', '--skip_problem_sources', action = 'store_true', 
        help = 'skip the unmirrored-sources output')
parser.add_argument('-m', '--skip_problem_mirrors', action = 'store_true', 
        help = 'skip the unused-mirrors output')
parser.add_argument('--input_mirrors_filename',   default = default_input_mirrors_filename, 
        help = "The output from get_mirrors_list.py")
parser.add_argument('--input_urls_filename',      default = default_input_urls_filename, 
        help = "The output from get_source_urls.py")
parser.add_argument('--output_manifest_filename', default = default_output_manifest_filename, 
        help = " ")
parser.add_argument('--problem_sources_filename', default = default_problem_sources_filename, 
        help = " ")
parser.add_argument('--problem_mirrors_filename', default = default_problem_mirrors_filename, 
        help = " ")
parser.add_argument('--allow_list_filename', default = default_allow_list_filename, 
        help = " ")
args=parser.parse_args()


if (args.skip_main_manifest and args.skip_problem_mirrors) and args.skip_problem_sources:
    print("You have selected options -l, -s and -m, which means the script will not produce any output")
    print("Please remove at least one option")
    raise SystemExit




allow_frame  = pd.read_csv(args.allow_list_filename, dtype='str')
mirror_frame = pd.read_csv(args.input_mirrors_filename, dtype='str')
urls_frame   = pd.read_csv(args.input_urls_filename, dtype='str')
merged_frame = pd.merge(urls_frame, mirror_frame, left_on = 'mirror_url', right_on = 'https_URL', how='outer')
merged_frame = merged_frame.sort_values(
        ['kind', 'label', 'alias', 'end_of_url', 'is_pull_mirror'], 
    )

exit_status = 0
if args.skip_main_manifest:
    print("Not producing a main mainifest. No changes made to: " + args.output_manifest_filename)
else:
    manifest_frame = merged_frame[
            pd.notna(merged_frame['label']) & pd.notna(merged_frame['mirror-type'])
        ]
    manifest_frame = manifest_frame.drop(columns=['https_URL'])
    manifest_frame.to_csv(args.output_manifest_filename, index = False)
    print('Writing manifest to: {}'.format(args.output_manifest_filename))

if args.skip_problem_sources:
    print("Not producing a list of unmirrored sources. No changes made to: " 
            + args.problem_sources_filename)
else:
    problem_sources_frame = merged_frame[pd.isna(merged_frame['mirror-type'])]
    problem_sources_frame = problem_sources_frame.drop(columns = list(mirror_frame.columns))
    for row in allow_frame.iterrows():
        allow_key = row[1]['field']
        allow_value = row[1]['allow-value']
        if allow_key in problem_sources_frame:
            problem_sources_frame = problem_sources_frame[ problem_sources_frame[allow_key] != allow_value ]
    problem_sources_frame.to_csv(args.problem_sources_filename, index = False)
    if len(problem_sources_frame.index) == 0:
        print('All sources appear to be mirrored')
        print('Writing empty list to: {}'.format(args.problem_sources_filename))
    else:
        print(red_ + 'Writing unmirrored sources list to: ' + unred_ +'{}'.format(args.problem_sources_filename))
        exit_status = 1
        unaliased_sources     = problem_sources_frame[ pd.isna(problem_sources_frame['alias'])]
        other_problem_sources = problem_sources_frame[pd.notna(problem_sources_frame['alias'])]
        if len(unaliased_sources.index) != 0:
            print(red_ + '{} unaliased sources found'.format(len(unaliased_sources.index)) + unred_)
            print(red_ + 'All source urls need to have an appropriate alias, for buildstream mirroring to work.' + unred_)
            if args.verbose:
                print('Displaying first 3 issues:')
                for index in unaliased_sources.index[:3]:
                    source = unaliased_sources.loc[index]
                    print('--------------------------')
                    print('Branch:              {}'.format(source['label']))
                    print('Element:             {}'.format(source['element']))
                    print('Alias:               {}'.format(source['alias']))
                    print('Origin URL:          {}'.format(source['source_url']))
                print('--------------------------')

        if len(other_problem_sources.index) != 0:
            print(red_ + '{} unmirrored sources found'.format(len(other_problem_sources.index)) + unred_)
            if args.verbose:
                print('Displaying first 3 issues:')
                for index in other_problem_sources.index[:3]:
                    source = other_problem_sources.loc[index]
                    print('--------------------------')
                    print('Branch:              {}'.format(source['label']))
                    print('Element:             {}'.format(source['element']))
                    print('Alias:               {}'.format(source['alias']))
                    print('Origin URL:          {}'.format(source['source_url']))
                    print('Expected mirror URL: {}'.format(source['mirror_url']))
                print('--------------------------')

if args.skip_problem_mirrors:
    print("Not producing a list of unused mirrors. No changes made to: " 
            + args.problem_mirrors_filename)
else:
    problem_mirrors_frame = merged_frame[pd.isna(merged_frame['label'])]
    problem_mirrors_frame = problem_mirrors_frame.drop(columns = list(urls_frame.columns))
    for row in allow_frame.iterrows():
        allow_key = row[1]['field']
        allow_value = row[1]['allow-value']
        if allow_key in problem_mirrors_frame:
            problem_mirrors_frame = problem_mirrors_frame[ problem_mirrors_frame[allow_key] != allow_value ]
    problem_mirrors_frame.to_csv(args.problem_mirrors_filename, index = False)
    if len(problem_mirrors_frame.index) == 0:
        print('All mirrors appear to be used in one or more relevant branches')
        print('Writing empty list to: {}'.format(args.problem_mirrors_filename))
    else:
        exit_status = 1
        print(red_ + '{} unused mirrors found'.format(len(problem_mirrors_frame.index)) + unred_)
        print(red_ + 'Writing unused mirrors list to: ' + unred_ +'{}'.format(args.problem_mirrors_filename))
        if args.verbose:
            print('Displaying first 3 issues:')
            for index in problem_mirrors_frame.index[:3]:
                mirror = problem_mirrors_frame.loc[index]
                print('--------------------------')
                print('Name: {}',format(mirror['name']))
                print('Type: {}'.format(mirror['mirror-type']))
                print('URL:  {}'.format(mirror['https_URL']))
sys.exit(exit_status)
