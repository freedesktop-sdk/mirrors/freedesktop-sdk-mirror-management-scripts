import os
import traceback
import pandas as pd
import argparse
from ruamel.yaml import YAML

default_output_file_name = 'list_of_freedesktop-sdk_source_urls.csv'
expected_mirror_url_start = 'https://gitlab.com/freedesktop-sdk/mirrors/'

green_  = u'\u001b[32m'
ugreen_ = u'\u001b[0m'

DESCRIBE_TEXT = '''
Creates a list of source URLs for the freedesktop-SDK project
Requires a copy of the repository to be accessible on the local machine
Reads all of the files in the 'elements' directory, and parses the YAML
Also reads the alias and mirror-alias definitions, to work out the correct full URLs
Outputs the component filename, alias, source URL and mirror URL, along with source type
Outputs the results as a csv file

Will only collect URLs from whichever branch is currently checked out. To collect information from multiple branches into one file, start with a blank output file, and run the script in append (-a) mode, checking out a new branch each time before re-running the script. Use the -l option to label output rows with the correct branch name.
'''

parser = argparse.ArgumentParser(
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description = DESCRIBE_TEXT
)

parser.add_argument('repo_path', 
        help = 'file path to the freedesktop-sdk repository folder in the local directory. Should end with "freedesktop-sdk/"')
parser.add_argument('-o', '--output_filename', default = default_output_file_name, 
        metavar = 'OUTPUT_FILENAME.csv', 
        help = 'filename for the csv output file. Defaults to ' + default_output_file_name)
parser.add_argument('-a', '--append', action='store_true',  
        help = 'append new lines to the output file, instead of overwriting it. Useful if you want to collect the source URLs from multiple branches of the repository.')
parser.add_argument('-l', '--label', nargs=1, default = [""], 
        help = 'adds a new field to every output row, containing the text in "label". Useful if running the script multiple times in append mode (eg to collect from multiple branches). The output lines from each runthrough can be distinguished by applying different labels each time.')
args=parser.parse_args()

if not args.repo_path.endswith('/'):
    args.repo_path += '/'
if not args.repo_path.endswith('freedesktop-sdk/'):
    print('Repository path needs to point to the freedesktop repository on the local machine. \nPath should end with "freedesktop-sdk/"')
    raise SystemExit
output_file_name = 'list_of_source_urls.csv'

writemode = 'a' if args.append else 'w'
alias_main_path = args.repo_path + 'include/_private/aliases.yml'
mirror_main_path = args.repo_path + 'include/_private/mirrors.yml'
project_conf_path = args.repo_path + 'project.conf'

yaml=YAML(typ='safe')
alias_dict = {}
if os.path.exists(alias_main_path):
    with open(alias_main_path, mode='r') as aliases_file:
        alias_dict = yaml.load(aliases_file)
else:
    with open(project_conf_path, mode='r') as aliases_file:
        alias_dict = yaml.load(aliases_file)['aliases']

mirror_list = {}
if os.path.exists(mirror_main_path):
    with open(mirror_main_path, mode='r') as mirrors_file:
        mirror_list = yaml.load(mirrors_file)['mirrors']
else:
    with open(project_conf_path, mode='r') as mirrors_file:
        mirror_list = yaml.load(mirrors_file)['mirrors']

mirror_dict = {}
for mirror_set in mirror_list:
    for key, value_list in mirror_set['aliases'].items():
        if key not in mirror_dict:
            mirror_dict[key] = value_list[0]
        else:
            if not mirror_dict[key].startswith(expected_mirror_url_start):
                mirror_dict[key] = value_list[0]
        #if there are conflicting candidates, give preference to the one that starts with our expected mirror URL

def apply_alias_to_url(input_url):
    output_alias = ''
    for try_alias, alias_url in alias_dict.items():
        if (try_alias + ':') in input_url:
            output_alias = try_alias + ":"
            output_url   = input_url.replace(output_alias, '')
            output_source_url = alias_dict[try_alias] + output_url
            output_mirror_url = mirror_dict.get(try_alias, '')
            output_mirror_url += '' if output_mirror_url == '' else output_url
            return output_alias, output_url, output_source_url, output_mirror_url
    return "", input_url, input_url, ''


url_list = []
counter = 0
for root, dirs, files in os.walk(args.repo_path + "elements/", followlinks=False):
    for name in files:
        if name[0] == ".":
            continue
        filename = os.path.join(root, name)
        output_filename = filename.replace(args.repo_path + "elements/", "")
        try:
            if (os.path.isfile(filename)):
                with open(filename, mode='r') as current_file:
                    yaml_source_list = yaml.load(current_file).get('sources', [])
                    for source_dict in yaml_source_list:
                        if 'submodules' in source_dict:
                            for mod_name, mod_details in source_dict['submodules'].items():
                                submod_info = {}
                                submod_info['element']    = output_filename
                                submod_info['kind']       = 'git_submodule'
                                submod_info['end_of_url'] = mod_details['url']
                                url_list.append(submod_info)
                        if 'url' in source_dict:
                            source_info = {}
                            source_info['element']    = output_filename
                            source_info['kind']       = source_dict['kind']     
                            source_info['end_of_url'] = source_dict['url']
                            url_list.append(source_info)
                        if '(?)' in source_dict:
                            for arch_dict in source_dict['(?)']:
                                for target_arch, value_dict in arch_dict.items():
                                    source_info = {}
                                    source_info['element']    = output_filename
                                    source_info['kind']       = source_dict['kind']
                                    source_info['end_of_url'] = value_dict['url']
                                    url_list.append(source_info)

        except Exception as e:
            print("Error while processing {}".format(filename))
            traceback.print_exc()


for source_info in url_list:
    url = source_info['end_of_url']
    alias, url, source_url, mirror_url = apply_alias_to_url(url)

    if source_url.startswith('https://gitlab.com/freedesktop-sdk/mirrors/'):
        mirror_url = source_url
        source_url = ''

    source_info['label']      = args.label[0]
    source_info['alias']      = alias
    source_info['end_of_url'] = url
    source_info['source_url'] = source_url
    source_info['mirror_url'] = mirror_url


column_names = ['label', 'kind', 'alias', 'end_of_url', 'element', 'source_url', 'mirror_url']
url_frame = pd.DataFrame(url_list, columns=column_names)
url_frame = url_frame.sort_values(['kind', 'alias', 'end_of_url'])
write_header = False if (writemode == 'a' and os.path.exists(args.output_filename)) else True
url_frame.to_csv(args.output_filename, index = False, mode = writemode, header = write_header)

print(green_ + 'Writing to:' + ugreen_ + '{}'.format(args.output_filename))
