import argparse
import gitlab
import pandas
import os

default_output_file_name = 'list_of_freedesktop-sdk_mirrors.csv'

gitlab_url      = ('https://gitlab.com')
mirror_group_id = '3487254'
tar_project_id  = '15704256'
tar_url_prefix  = 'https://gitlab.com/freedesktop-sdk/mirrors/tarball-lfs-mirror/raw/master/'

description_mirror_group_url = 'https://gitlab.com/groups/freedesktop-sdk/mirrors'
description_tar_project_url  = 'https://gitlab.com/freedesktop-sdk/mirrors/tarball-lfs-mirror.git'


DESCRIBE_TEXT = '''
Uses the gitlab API to collect a list of the freedesktop-sdk gitlab source mirrors.
Outputs the list as a CSV file, containing each mirror's name, id, and the relevant URL to access it.
Specifically, at time of writing, these mirrors are hosted as follows:
    Gitlab instance:  ''' + gitlab_url + '''
    Project group:    ''' + description_mirror_group_url + '''
    Project group id: ''' + mirror_group_id + '''
All of the various git repositories that we mirror, are hosted in this group and its subgroups. We also have a single git LFS repo for hosting individual file sources (tarballs, zip archives, and any other single file sources.)
    Git LFS repo:     ''' + description_tar_project_url + '''
    Project id:       ''' + tar_project_id + '''
These values are coded into the script, and will need to be changed if we move the repository.
'''

parser = argparse.ArgumentParser(
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description = DESCRIBE_TEXT
)
parser.add_argument('-o', '--output_filename', default = default_output_file_name,
        metavar = 'OUTPUT_FILENAME.csv',
        help = 'filename for the csv output file. Defaults to ' + default_output_file_name)
args=parser.parse_args()



gl = gitlab.Gitlab(gitlab_url)
print("Contacting Gitlab instance")
mirror_group = gl.groups.get(mirror_group_id)
print("Getting list of git mirrors from {}".format(mirror_group.web_url))
projects = mirror_group.projects.list(all = True, include_subgroups = True)
tar_project = gl.projects.get(tar_project_id)

columns = ["name", "mirror-id", "mirror-type", "is_pull_mirror", "https_URL"]

output_list = []
for project in projects:
    if project.id == int(tar_project_id):
        continue
    project_dict = {
        'name':             project.name, 
        'mirror-id':        project.id, 
        'mirror-type':     'git repo',
        'is_pull_mirror':   project.mirror, 
        'https_URL':        project.http_url_to_repo
    }
    output_list.append(project_dict)

print("Getting list of file mirrors from {}".format(tar_project.http_url_to_repo))
tar_list = tar_project.repository_tree(recursive = True, all=True)
for tar_file in tar_list:
    if tar_file['name'] in ['.gitattributes', 'README.md']:
        continue
    if tar_file['type'] == 'tree':
        continue
    tar_dict = {
        'name':         tar_file['name'], 
        'mirror-id':    tar_file['id'], 
        'mirror-type': 'file download',
        'https_URL':    tar_url_prefix + tar_file['path']
    }
    output_list.append(tar_dict)

mirror_frame = pandas.DataFrame(output_list, columns=columns)
mirror_frame.to_csv(args.output_filename, index=False)

print("Writing to {}".format(args.output_filename))
